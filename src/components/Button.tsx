interface ButtonProps {
  caption: string;
  color?: "primary" | "secondary" | "danger";
  onClickHandler: () => void;
}

function Button({ caption, color = "primary", onClickHandler }: ButtonProps) {
  return (
    <button
      type="button"
      className={"btn btn-" + color}
      onClick={onClickHandler}
    >
      {caption}
    </button>
  );
}

export default Button;
