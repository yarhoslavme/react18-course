import { useState } from "react";
import { FcLike, FcDislike } from "react-icons/fc";

interface LikeProps {
  state?: boolean;
  onClickHandler: () => void;
}

function Like({ state = false, onClickHandler }: LikeProps) {
  const [active, setActive] = useState(state);

  const toggle = () => {
    setActive(!active);
    onClickHandler();
  };

  if (active) {
    return (
      <div>
        <FcLike onClick={toggle} />
      </div>
    );
  }
  return (
    <div>
      <FcDislike onClick={toggle} />
    </div>
  );
}

export default Like;
