import { ReactNode } from "react";

interface AlertProps {
  children: ReactNode;
  type?: "primary" | "secondary" | "success" | "danger" | "warning";
  onCloseHandler: () => void;
}

function Alert({ children, type = "primary", onCloseHandler }: AlertProps) {
  return (
    <div
      className={"alert-dismissible fade show alert alert-" + type}
      role="alert"
    >
      {children}
      <button
        type="button"
        className="btn-close"
        data-bs-dismiss="alert"
        aria-label="Close"
        onClick={onCloseHandler}
      ></button>
    </div>
  );
}

export default Alert;
