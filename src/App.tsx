import ListGroup from "./components/ListGroup";
import Button from "./components/Button";
import { useState } from "react";
import Alert from "./components/Alert";
import Like from "./components/Like";

const cities = ["Toronto", "Kitchener", "Waterloo"];

function App() {
  const [alerVisible, setAlertVisible] = useState(false);

  return (
    <div>
      {alerVisible && (
        <Alert onCloseHandler={() => setAlertVisible(false)}>
          Este es un ejemplo de Alert!!!
        </Alert>
      )}
      <ListGroup
        items={cities}
        heading="Cities"
        onSelectItem={(item) => console.log(item)}
      />
      <Button
        caption="Close"
        color="danger"
        onClickHandler={() => {
          console.log("Clicked");
          setAlertVisible(true);
        }}
      />
      <Like onClickHandler={() => console.log("Like-Clicked")}></Like>
    </div>
  );
}

export default App;
